import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthGuard } from '../auth.guard';
import { NftService } from '../nft.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private nftService: NftService,private router: Router) { }

  ngOnInit(): void {

  }
  validateRegister(RegisterForm: any){
    // console.log(RegisterForm)
    this.nftService.registerUser(RegisterForm).subscribe((data: any)=>{
      console.log(data)
      if (data.status == 200){
        this.router.navigate(['login'])
        // this.guard.isLoggedIn=true;


      }
      else{
         this.router.navigate(['register'])
          // this.guard.isLoggedIn=false;
      }
        
    })
  }

}
