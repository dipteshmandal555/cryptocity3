import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { AuthGuard } from '../auth.guard';
import { NftService } from '../nft.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  search: String = ""
 


  constructor(private guard: AuthGuard, private router: Router,private nftService: NftService) { 

  }

  ngOnInit(): void {



  }
  logout(){
    this.nftService.logOut()

  }
  // search(){

  // }
  @Output()
  searchText: EventEmitter<String> = new EventEmitter<String>();
onSearchTextChanged(){
  this.searchText.emit(this.search)
  console.log(this.searchText)
}
}
