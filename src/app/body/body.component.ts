import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NftService } from '../nft.service';
declare var window:any;
@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css',]
})
export class BodyComponent implements OnInit {
  body: any;
  bid:boolean;
  price:any;
     formModal:any;
  constructor(private nftService: NftService,private router: Router) {
    this.bid = false;
    // this.price = any

   }

  ngOnInit() {
    this.body = this.nftService.getNftsData()
     this.formModal =  new window.bootstrap.Modal(
document.getElementById("myModal"))
     console.log(this.body)
     this.price = this.body.productprice
      console.log(this.price)
    // this.nftService.getNftsData().subscribe((data:any)=>{
    //   console.log(data)
    //       // this.body =data
          
    // })
  
  }
  bidUp(id:number,price:String){
    this.nftService.bidNft(id,price).subscribe((data:any)=>{
      this.openModal()
      console.log(data)
    })
    

    this.bid = !this.bid;
    console.log(this.bid)
  }
     openModal(){
    this.formModal.show();
  }
  hide(){
    this.formModal.hide();
  }



}
