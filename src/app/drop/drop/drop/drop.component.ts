import { Component, OnInit } from '@angular/core';
import { NftService } from 'src/app/nft.service';

@Component({
  selector: 'app-drop',
  templateUrl: './drop.component.html',
  styleUrls: ['./drop.component.css']
})
export class DropComponent implements OnInit {
  cards:any
  constructor(private nftService: NftService) {
    this.cards=[]
   }

  ngOnInit(): void {
        this.nftService.getNfts().subscribe((data:any)=>{
        this.cards=data;
        console.log(data);
    })
  }

}
