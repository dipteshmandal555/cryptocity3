import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { AuthGuard } from './auth.guard';
import { BidComponent } from './bid/bid.component';
import { BodyComponent } from './body/body.component';
import { DetailComponent } from './detail/detail.component';
import { DropComponent } from './drop/drop/drop/drop.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { MarketPlaceComponent } from './market-place/market-place.component';

import { RegisterComponent } from './register/register.component';
// ,canActivate:[AuthGuard]
const routes: Routes = [
    {path:'home',component:HomeComponent},

  {path:'MarketPlace',component:MarketPlaceComponent},
  {path:'body',canActivate:[AuthGuard],component:BodyComponent},
  {path:"",component:LoginComponent},
   {path:"register",component:RegisterComponent},
  {path:"channel",canActivate:[AuthGuard],component:DetailComponent},
  {path:"about",component:AboutComponent},
    {path:"bid",canActivate:[AuthGuard],component:BidComponent},
    {path:"drop",canActivate:[AuthGuard],component:DropComponent},



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
