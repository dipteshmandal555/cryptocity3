import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthGuard } from '../auth.guard';
import { NftService } from '../nft.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private nftService: NftService,private guard:AuthGuard,private router:Router) { }

  ngOnInit(): void {
    

  }
  LoginUser(LoginForm:any){
    console.log(LoginForm)
    this.nftService.loginUser(LoginForm).subscribe((data:any) => {
      console.log(data);
       if (data.status == 200){
        console.log(data)
        this.router.navigate(['MarketPlace'])
        this.guard.isLoggedIn=true;


      }
      else{
         console.log(data)
         this.router.navigate(['register'])
          this.guard.isLoggedIn=false;
      }
        
    })
  }

}
