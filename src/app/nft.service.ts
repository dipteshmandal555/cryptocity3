import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http';
import { AuthGuard } from './auth.guard';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class NftService {
  static getUsers() {
    throw new Error('Method not implemented.');
  }
  static getNfts() {
    throw new Error('Method not implemented.');
  }
  static registerUser(){
     throw new Error('Method not implemented.');
  }
  static loginUser(){
     throw new Error('Method not implemented.');
  }
  static getNftsData() {
  throw new Error('Method not implemented.');
 }
 static setNftsData() {
  throw new Error('Method not implemented.');
 }
 static setChannelData() {
  throw new Error('Method not implemented.');
 }
 static getchannelData() {
  throw new Error('Method not implemented.');
 }
 static getNftByUser(){
   throw new Error('Method not implemented.');
 }
  static bidNft(){
   throw new Error('Method not implemented.');
 }
 static logOut(){
    throw new Error('Method not implemented.');
 }
  NftData:any
  channelData:any;
  constructor(private httpClient: HttpClient,private guard:AuthGuard,private router:Router) { }

  getUsers(){
    return this.httpClient.get("http://localhost:3000/usersDetails")
  }
  getNfts(){
    return this.httpClient.get("http://localhost:3000/getNfts")
  }
  
  registerUser(RegisterForm:any){
    return this.httpClient.post(
      "http://localhost:3000/register",
        RegisterForm
      )

  }
  loginUser(LoginForm:any){
     return this.httpClient.post(
      "http://localhost:3000/login",
        LoginForm
      )
  }
  getNftByUser(nfts:any){
    return this.httpClient.post(
      "http://localhost:3000/getNftsById",
       { "NFTS":nfts}
      )

  }
 setNftData(data:any){
    this.NftData = data;
 } 
 getNftsData() {
    return this.NftData;
 }
 setChannelData(data:any){
    this.channelData = data;
 }
 getChannelData(){
    return this.channelData;
 }
 bidNft(id:Number,price:String){
  console.log(id,price);
   return this.httpClient.patch("http://localhost:3000/bid/"+id,{ "productprice":price})
 }
 logOut(){
     this.httpClient.post("http://localhost:3000/logout",{}).subscribe((data:any)=>{
      if(data.status == 200){
        this.guard.isLoggedIn = false;
        this.router.navigate([
          ""
        ])

      }else{
        this.guard.isLoggedIn = false;
      }
     })
 }
 
}
