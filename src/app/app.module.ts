import { BidComponent } from './bid/bid.component';
// import { CommonModule } from '@angular/common';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, } from '@angular/core';
// import { FormsModule } from '@angular/forms';
import { BrowserModule, } from '@angular/platform-browser';
import { AboutComponent } from './about/about.component';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BodyComponent } from './body/body.component';
import { DetailComponent } from './detail/detail.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { MarketPlaceComponent } from './market-place/market-place.component';
import { RegisterComponent } from './register/register.component';
import { DigitalClockComponent } from './digital-clock/digital-clock.component';
import { DropComponent } from './drop/drop/drop/drop.component';
import { AuthGuard } from './auth.guard';
import { FormsModule } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MarketPlaceComponent,
    RegisterComponent,
    LoginComponent,
    BodyComponent,
    DetailComponent,
    HomeComponent,
    AboutComponent,
    BidComponent,
    DigitalClockComponent,
    DropComponent,
    // PopupComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    HttpClientModule,
     FormsModule,
    // NgbModal
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
