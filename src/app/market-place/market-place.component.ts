import { Component, OnChanges, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthGuard } from '../auth.guard';
import { NftService } from '../nft.service';
declare var window:any;
@Component({
  selector: 'app-market-place',
  templateUrl: './market-place.component.html',
  styleUrls: ['./market-place.component.css']
})
export class MarketPlaceComponent implements OnInit  {
  mobiles:any;
 cards:any
 searchText:String ="";
 popup:boolean = true;

   formModal:any;
  
  constructor(private router:Router,private nftService:NftService, private auth:AuthGuard) {

    this.mobiles=[]
  this.cards=[]
    // this.cards=[{cardid:1,productedition:"4.8k/21.9 Editions for sale",product_name:"Brilliant Gold",productprice:"currentbid: $1     |     Ends in :4days",productdomain:"crypto.com",imgpath:"../../assets/images/card1.png"},
    // {cardid:2,productedition:"1k/9 Editions for sale",product_name:"Psycho kitty Nelson",productprice:"currentbid: $2    |     Ends in :4days",productdomain:"crypto.com",imgpath:"../../assets/images/card2.png"},
    // {cardid:2,productedition:"2k/2.9 Editions for sale",product_name:"Peace kitty",productprice:"currentbid: $19     |     Ends in :16days",productdomain:"crypto.com",imgpath:"../../assets/images/card3.png"},
    // {cardid:3,productedition:"3.1k/1.9 Editions for sale",product_name:"Opening night",productprice:"floorcurrentbid: $14     |     Ends in :12days",productdomain:"crypto.com",imgpath:"../../assets/images/card4.png"},
    // {cardid:4,productedition:"5k/4 Editions for sale",product_name:"Loaded Lions",productprice:"currentbid: $10     |     Ends in :45days",productdomain:"crypto.com",imgpath:"../../assets/images/card5.png"},
    // {cardid:5,productedition:"21k/2.3 Editions for sale",product_name:"Kaliju Legends",productprice:"floorPrice currentbid: $12     |     Ends in :3days",productdomain:"crypto.com",imgpath:"../../assets/images/card6.png"},
    // {cardid:6,productedition:"33k/3.6 Editions for sale",product_name:"cyber club",productprice:"floorPrice currentbid: $12     |     Ends in :8days",productdomain:"crypto.com",imgpath:"../../assets/images/card8.png"},
    // {cardid:7,productedition:"12k/7.8 Editions for sale",product_name:"BeatHeadz",productprice:"floorPrice currentbid: $33     |     Ends in :7days",productdomain:"crypto.com",imgpath:"../../assets/images/card7.png"},
    // {cardid:8,productedition:"7k/8.7 Editions for sale",product_name:"A Heavy Metal Christ",productprice:"floorPrice currentbid: $67     |     Ends in :9days",productdomain:"crypto.com",imgpath:"../../assets/images/card10.png"},
    // {cardid:9,productedition:"12k/21.9 Editions for sale",product_name:"Mad Hare",productprice:"floorPrice currentbid: $17     |     Ends in :3days",productdomain:"crypto.com",imgpath:"../../assets/images/card9.png"},
    // {cardid:10,productedition:"4.22k/21.9 Editions for sale",product_name:"Ballie #232",productprice:"floorPrice currentbid: $35     |     Ends in :2days",productdomain:"crypto.com",imgpath:"../../assets/images/ard11.png"},
    // {cardid:11,productedition:"6.7k/21.9 Editions for sale",product_name:"Kaiju legend",productprice:"floorPrice currentbid: $12     |     Ends in :5days",productdomain:"crypto.com",imgpath:"../../assets/images/card11.png"},
    // {cardid:2,productedition:"5.5k/21.9 Editions for sale",product_name:"Kitty ii",productprice:"floorPrice currentbid: $87     |     Ends in :4days",productdomain:"crypto.com",imgpath:"../../assets/images/card12.png"},
    // {cardid:3,productedition:"4k/21.9 Editions for sale",product_name:"Cronos -7 Vositor",productprice:"floorPrice currentbid: $32     |     Ends in :8days",productdomain:"crypto.com",imgpath:"../../assets/images/card13.png"},
    // {cardid:4,productedition:"12k/21.9 Editions for sale",product_name:"Kinetica Miami",productprice:"floorPrice currentbid: $100     |     Ends in :4days",productdomain:"crypto.com",imgpath:"../../assets/images/card1.png"},
    // {cardid:5,productedition:"4k/21.9 Editions for sale",product_name:"Loaded Lions",productprice:"floorPrice currentbid: $17     |     Ends in :1days",productdomain:"crypto.com",imgpath:"../../assets/images/card2.png"},
    // {cardid:6,productedition:"4.8k/21.9 Editions for sale",product_name:"Angels & Demon",productprice:"floorPrice currentbid: $21     |     Ends in :2days",productdomain:"crypto.com",imgpath:"../../assets/images/card3.png"},
    // {cardid:6,productedition:"4.8k/21.9 Editions for sale",product_name:"Word Wizard",productprice:"floorPrice currentbid: $12     |     Ends in :1days",productdomain:"crypto.com",imgpath:"../../assets/images/card4.png"},
    // {cardid:6,productedition:"4.8k/21.9 Editions for sale",product_name:"Og jack",productprice:"floorPrice currentbid: $10     |     Ends in :8days",productdomain:"crypto.com",imgpath:"../../assets/images/card5.png"}]
  
    
   }

  ngOnInit() {
    this.popup = !this.auth.isLoggedIn;
    console.log(this.popup)
       this.formModal =  new window.bootstrap.Modal(
document.getElementById("myModal"))
    this.nftService.getUsers().subscribe((data:any)=>{
        this.mobiles=data;
        console.log(data);
    })
     this.nftService.getNfts().subscribe((data:any)=>{
        this.cards=data;
        console.log(data);
    })
  }
  nftDetails(data:any) {
  console.log(this.auth.isLoggedIn)
  if(this.auth.isLoggedIn === false){
      this.openModal()
      }else{
       
           try{
      // console.log(data);
       this.nftService.setNftData(data)
    this.router.navigate(['/body'])
    }catch(err){
       console.log(err)
    }
      }
 
 
  }
channelDetails(data:any) {
  if(this.auth.isLoggedIn === false){
      this.openModal()
      }else{
try{
      // console.log(data);
  this.nftService.setChannelData(data)
  this.router.navigate(['/channel'])
    }catch(err){
       console.log(err)
    }
        
      }

    
 
    
  }
  onSearchTextEntered(searchValue:String) {
      this.searchText = searchValue;
      console.log(this.searchText)
  }
  search(){
//     if(this.searchText == ""){
// this.ngOnInit()
//     }else{
      
//     }
    
  }
    openModal(){
    this.formModal.show();
  }
  hide(){
    this.formModal.hide();
  }

}
