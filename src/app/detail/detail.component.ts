import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NftService } from '../nft.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  mobile:any;
  cards:any
  constructor(private nftService: NftService,private router: Router) { 
    // this.mobiles=[]
  
    this.cards=[]

  }

  ngOnInit(): void {
    this.mobile = this.nftService.getChannelData()
    console.log(this.mobile)
    this.nftService.getNftByUser(this.mobile.NFTS).subscribe((data)=>{
        this.cards = data
        // console.log(data)
    })

  }
    nftDetails(data:any) {
    try{
      console.log(data);
  this.nftService.setNftData(data)
    this.router.navigate(['body'])
    }catch(err){
       console.log(err)
    }
 
  }

}
